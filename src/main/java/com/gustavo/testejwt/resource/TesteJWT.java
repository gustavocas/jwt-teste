package com.gustavo.testejwt.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.testejwt.configJWT.Usuario;
import com.gustavo.testejwt.configJWT.UsuarioService;

@RestController
@RequestMapping(value="/teste")
public class TesteJWT {
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping(value="/buscar")
	public ResponseEntity<Usuario> findByEmail(@RequestParam String email){
		
		Usuario u = null;//(Usuario) usuarioService.loadUserByUsername(email);
		return ResponseEntity.ok(u);
		
	}
	
	@GetMapping(value="/testar")
	public ResponseEntity<Object> findByNome(@RequestParam String email){
		
		Object u = email;
		return ResponseEntity.ok(u);
		
	}
}
