package com.gustavo.testejwt.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gustavo.testejwt.configJWT.Usuario;
import com.gustavo.testejwt.configJWT.UsuarioService;

@RestController
@RequestMapping(value="/operador")
public class TesteJWTOperador {
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping(value="/teste")
	public ResponseEntity<Object> findByNome(){
		
		Object u = "Teste OK";
		return ResponseEntity.ok(u);
		
	}
}
