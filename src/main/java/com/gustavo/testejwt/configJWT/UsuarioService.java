package com.gustavo.testejwt.configJWT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService implements UserDetailsService{

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usu = null;
		if (username.equals("admin")) {
			usu = new Usuario(1,"admin","email",bCryptPasswordEncoder.encode("senha"));
			usu.getRegras().add(new Regras(1,"ROLE_ADMIN"));
		}else if (username.equals("gugu")) {
			usu = new Usuario(1,"gugu","email",bCryptPasswordEncoder.encode("senha"));
			usu.getRegras().add(new Regras(1,"ROLE_OPERATOR"));
		}else {
			throw new UsernameNotFoundException("Usuarie nao encontradis");
		}
		 
		return usu;
	}
}
